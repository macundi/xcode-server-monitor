//
//  XcodeServer_MonitorTests.swift
//  XcodeServer MonitorTests
//
//  Created by Markus Stöbe on 12.03.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import XCTest
@testable import XcodeServer_Monitor

class XcodeServer_MonitorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
