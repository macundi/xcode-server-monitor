//
//  DetailViewController.swift
//  XcodeServer Monitor
//
//  Created by Markus Stöbe on 12.03.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
	@IBOutlet weak var integrationCount: UILabel!


    var detailItem: Bot? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.name
            }
			if let label = self.integrationCount {
				label.text = "Integration Count:\n\(detail.integrationCount)"
			}
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

	@IBAction func runIntegration() {
		//if detailitem is set, readout the baseURL and start a new integration
		if let thisBot = self.detailItem {
			BackendHandler.sharedInstance.startIntegration(forBot: thisBot)
		}
	}
}

