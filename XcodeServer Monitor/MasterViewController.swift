//
//  MasterViewController.swift
//  XcodeServer Monitor
//
//  Created by Markus Stöbe on 12.03.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    //******************************************************************************************************************
    //MARK: - Properties and Outlets
    //******************************************************************************************************************
    var detailViewController: DetailViewController? = nil
    var bots = [Bot]()                        //this array will hold one bot-object for eacht bot found on the sever,
                                              //storing relevant info about each bot
    
    //******************************************************************************************************************
    //MARK: - Lifecycle
    //******************************************************************************************************************
    override func viewDidLoad() {
        super.viewDidLoad()

        //handle splitview stuff for iphone plus
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(animated: Bool) {
        //handle splitview stuff for iphone plus
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed

		super.viewWillAppear(animated)
		
		//init backend handler and load list of available bots
		bots = BackendHandler.sharedInstance.loadBotlistFromServer()
		self.tableView.reloadData()
    }

    //******************************************************************************************************************
    // MARK: - Segues
    //******************************************************************************************************************
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let thisBot = bots[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = thisBot
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    //******************************************************************************************************************
    // MARK: - Table View
    //******************************************************************************************************************
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bots.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let thisBot = bots[indexPath.row]
        cell.textLabel!.text = thisBot.name
        return cell
    }

}

