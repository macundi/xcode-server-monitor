//
//  BackendHandler.swift
//  XcodeServer Monitor
//
//  Created by Markus Stöbe on 12.03.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import Foundation

class BackendHandler: NSObject, NSURLSessionDelegate, NSURLSessionDataDelegate {
    //******************************************************************************************************************
    //MARK: - Properties
    //******************************************************************************************************************
    static let sharedInstance = BackendHandler()            //this makes the class a singleton
    var baseURL = "https://localhost:20343/api/bots/"   //baseURL for all calls to backend
                                                            //make sure to disable AppTransportSecurity 
                                                            //in info.plist for this to work!
    //******************************************************************************************************************
    //MARK: - Methods
    //******************************************************************************************************************
    func loadBotlistFromServer()->[Bot] {
        let data = NSData(contentsOfURL:NSURL(string:self.baseURL)!)
        return self.parseServerData(data)
    }
    
    func parseServerData(rawData:NSData?)->[Bot] {
        var botlist = [Bot]()       //make a new typed array for holding bot-objects
        
        if let dataToParse = rawData {
            //parse it
            do {
				let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(dataToParse, options:[]) as! NSDictionary
				//if there is an array full of stars...erm...bots...
				if let bots = jsonDictionary["results"] as? NSArray {
					//go through the list one by one
					for thisBot in bots  {
						//let's check a last time if this is a bot (sanity check, never saw another value)
						let docType = thisBot["doc_type"] as! String
						if  docType == "bot" {
							//then finally get all needed data from the bot and store it in bot-object
							let newBot = Bot()
							newBot.name = thisBot["name"] as! String
							newBot.id   = thisBot["_id"]  as! String
							newBot.integrationCount = (thisBot["integration_counter"] as! Int) - 1
							botlist.append(newBot)
						}
					}
                }
            } catch {
                print("error parsing JSON data from server: \(error)")
            }
			
        } else {
            //create one dummy bot in load-error case to prevent an empty list
            let newBot = Bot()
            newBot.name = "There was an error loading server data!"
            newBot.id   = "1234567890"
            newBot.integrationCount = 1
            botlist.append(newBot)
        }
		
		//return filled list
        return botlist
    }
	
	func startIntegration(forBot integrateBot:Bot) {
		//create the URL to start an new integrations
		//https://server.mycompany.com:20343/api/bots/6f0faeb1cc4c55e174cac5ff8100f13b/integrations
		let postURL = self.baseURL + integrateBot.id + "/integrations"
		
		//prepare an URLRequest for the task at hand
		let request = NSMutableURLRequest()
		request.HTTPMethod = "POST"
		request.HTTPBody   = "{shouldClean: false}".dataUsingEncoding(NSUTF8StringEncoding)
		request.URL        = NSURL(string:postURL)

		//use a new URLSession to perform the request
		let urlSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(),
			                               delegate: self,
									  delegateQueue: nil)
		
		//always use datatasks without completionblock, otherwise delegate funcs won't be called!!!
		let dataTask   = urlSession.dataTaskWithRequest(request)
		dataTask.resume()
	}
	
	//******************************************************************************************************************
	//MARK: - NSURLSessionDelegate
	//******************************************************************************************************************
	func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void)
	{
		if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust{
			let credential = NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!)
			completionHandler(NSURLSessionAuthChallengeDisposition.UseCredential,credential);
		}
	}
	
	func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
		//if task is finished, reload botlist from server
		BackendHandler.sharedInstance.loadBotlistFromServer()
	}
	

}
