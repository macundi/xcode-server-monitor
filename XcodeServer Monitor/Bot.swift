//
//  Bot.swift
//  XcodeServer Monitor
//
//  Created by Markus Stöbe on 12.03.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//
//******************************************************************************************************************
//* storage class to persist info we need about a bot for the duration of the runtime
//* values will be filled in by the parser inside BackendHandler.swift!
//******************************************************************************************************************

import Foundation

class Bot: NSObject {
    //******************************************************************************************************************
    //MARK: - Properties
    //******************************************************************************************************************

    var name :String            //user readable name of the bot
    var id   :String            //id the identifies the bot on our server (hex-string)
    var integrationCount = 0    //number of integrations

    //******************************************************************************************************************
    //MARK: - Lifecylce
    //******************************************************************************************************************
    override init() {
        self.name = "empty bot"
        self.id   = ""

        super.init()
    }
}